# Chronicler
Interconnected notes; Roadmap/Timeline; Milestones

# Priority

1. ~~[Development environment](#development-environment)~~
2. [CI/CD](#cicd)
3. [Web UI](#web-ui)
4. [API](#api)
5. [CLI](#cli)
6. [Other tools](#other-tools)

# Development Environment

- [Building the runway - React Code Smells by Thai](https://www.youtube.com/watch?v=xBa0_b-5XDw)
- ~~Visual Studio Online?~~ <- The [approx. cost](https://azure.microsoft.com/en-us/pricing/details/visual-studio-online/) don't excite me

# CI/CD

- [ ] Auto DevOps [by GitLab](https://docs.gitlab.com/ee/topics/autodevops/quick_start_guide.html)
- [ ] Kubernetes
- [ ] [Per-repository deploy keys](https://gitlab.com/help/ssh/README#per-repository-deploy-keys)
- /docs
  - [x] gitbook
  - [x] gitlab pages
  - [ ] domain/cert

# UI

### Web

- [x] [CRA+tailwind](https://blog.logrocket.com/create-react-app-and-tailwindcss/)
- Checking out: [reach/router](https://reach.tech/router)
- Display notes via URL
  - [x] Tried @reach/router
  - [ ] Use react-router v5
- [ ] Save to database via [API](#API)

##### Web UI Testing

- [ ] Jest
- [ ] Cypress

### CLI

- [ ] oclif

# API

### Database

- [ ] Postgres

### API testing

- [ ] [Polly](https://github.com/App-vNext/Polly/wiki/Unit-testing-with-Polly)

# Other notes

- [Efficient Software Management at its Roots](source: https://blog.pragmaticengineer.com/efficient-software-project-management-at-its-roots/)

